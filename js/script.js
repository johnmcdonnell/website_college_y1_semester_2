$(document).ready(function() {

	// Remove welcome message and rule diagram on game start
	$('.choice').click(function() {
		$('#info').animate({ opacity: 0 }, 300);
		$('#game').removeClass('rules', 300);
		$('.choice span').animate({ opacity: 0 }, 300);
	});

	// Add rule diagram again on user request
	$('.again').click(function() {
		$('#game').addClass('rules');
		$('.choice span').animate({ opacity: 1 }, 300);
		$('#info').animate({ opacity: 0 }, 300);
	});
	
	var choices = ['rock', 'paper', 'scissors', 'lizard', 'spock'];
	var userChoice = '';
	var cpuChoice = '';
	var wins = 0;
	var loses = 0;

	function result(userChoice, cpuChoice) {
		var result = '';

		if(userChoice == 'rock') {
			if (cpuChoice == 'rock') {
				result = 'Draw';
			} else if (cpuChoice == 'paper' || cpuChoice == 'spock') {
				result = 'Lose';
				loses++;
			} else {
				result = 'Win';
				wins++;
			};
		} else if(userChoice == 'paper') {
			if (cpuChoice == 'paper') {
				result = 'Draw';
			} else if (cpuChoice == 'scissors' || cpuChoice == 'lizard') {
				result = 'Lose';
				loses++;
			} else {
				result = 'Win';
				wins++;
			};
		} else if(userChoice == 'scissors') {
			if (cpuChoice == 'scissors') {
				result = 'Draw';
			} else if (cpuChoice == 'rock' || cpuChoice == 'spock') {
				result = 'Lose';
				loses++;
			} else {
				result = 'Win';
				wins++;
			};
		} else if(userChoice == 'lizard') {
			if (cpuChoice == 'lizard') {
				result = 'Draw';
			} else if (cpuChoice == 'rock' || cpuChoice == 'scissors') {
				result = 'Lose';
				loses++;
			} else {
				result = 'Win';
				wins++;
			};
		} else if(userChoice == 'spock') {
			if (cpuChoice == 'spock') {
				result = 'Draw';
			} else if (cpuChoice == 'paper' || cpuChoice == 'lizard') {
				result = 'Lose';
				loses++;
			} else {
				result = 'Win';
				wins++;
			};
		} else {
			return false;
		};

		return result;
	};

	// When a new game starts
	$('.choice').click(function() {
			userChoice = $(this).attr('id'); // Set userChoice to selection
	    	cpuChoice = choices[Math.floor(Math.random() * choices.length)]; // Set CPU to random selection
	    	gameResult = result(userChoice, cpuChoice);

			$('.choice').removeClass('win lose draw'); // Remove previous selection classes
			
			// Select class depending on result
			if(gameResult == 'Win') {
				// If user wins
				$(this).addClass('win'); 
	    		$('#choices').find('#'+cpuChoice).addClass('lose');
			} else if(gameResult == 'Lose') {
				// If user loses
				$(this).addClass('lose');
	    		$('#choices').find('#'+cpuChoice).addClass('win');
			} else {
				// If result is a draw
				$(this).addClass('draw');
			}

    	$('#info').animate({ opacity: 0 }, 50, function() {
    		$('#info').html(''); // Erase data
    		if(gameResult != 'Draw') {
    			$('#info').append('<h3>You ' + gameResult + '</h3>').animate({ opacity: 1 }, 50); // Output result
    		} else {
    			$('#info').append('<h3>' + gameResult + '</h3>').animate({ opacity: 1 }, 50); // Output result
    		}
	    	
	    	$('#info').append('<p>Wins: ' + wins + ' - Loses: ' + loses + '</p>').animate({ opacity: 1 }, 50); // Output total score
    	});
	});

}); // End $(document).ready

